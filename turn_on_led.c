#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>

int main(int argc, char **argv) 
{
	int fd;
	int on = 0;

	fd = open("/dev/led_driver", O_RDWR);

	if(fd < 0) {
		printf("Error opening the device!\n");
		printf("[%s]\n", strerror(errno));
	}

	else 
	{
		while(1) 
		{
			ioctl(fd, on);
		}
	}


	close(fd);

	// If there was an error closing the driver.
	if(fd < 0) 
	{
		printf("Error closing the device!\n");
		printf("[%s]\n", strerror(errno));
	}

	return 0;
}
