#include <linux/module.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/ioport.h>
#include <asm/io.h>
#include <linux/sched.h>
#include <linux/gpio.h>

MODULE_LICENSE("GPL");

#define SUCCESS 0
#define DEVICE_NAME "led_driver"

#define BUF_LEN 80

#define outputPin 4


static unsigned PORT = 0x3f000000;
static unsigned RANGE = 0x40;

u8 *addr;

static int Major;

static int Device_Open = 0;

static char msg[BUF_LEN];

static char *msg_Ptr;

int gpioRequestOutput;

int gpioDirectionRequestOutput;

int gpioRequestInput;

int gpioDirectionRequestInput;

// FUNCTION DECLARATIONS

static int led_driver_open(struct inode *, struct file *);
static int led_driver_release(struct inode *, struct file *);
static ssize_t led_driver_read(struct file *, char *, size_t, loff_t *);
static ssize_t led_driver_write(struct file *, const char *, size_t, loff_t *);

// FUNCTION DEFINITIONS

// Called when a process tries to open the device file.
static int led_driver_open(struct inode *inode, struct file *file) {


	msg_Ptr = msg;

	sprintf(msg,"Not implemented\n");

	if (Device_Open) 
	{

		printk("Device is already open.\n");

		return -EBUSY;
	}

	Device_Open++;

	try_module_get(THIS_MODULE);

	addr = ioremap(PORT, RANGE);

	if (addr  != NULL) 
	{
		// Request the output pin as output
		gpioRequestOutput = gpio_request(outputPin, "outputPinLabel");

		if(gpioRequestOutput != 0) 
		{
			printk("Error while setting the GPIO as an output!\n");
		}

		else 
		{
			// Try to drive the output pin low.
			gpioDirectionRequestOutput = gpio_direction_output(outputPin, 0);

			// If there was an error trying to initialize the output pin.
			if(gpioDirectionRequestOutput != 0) 
			{
				printk("Error while initializing the output!\n");
			}
		}

		return SUCCESS;
    }

	else 
	{
		printk("addr == NULL!\n");
	}

	return -ENODEV;
}


static int led_driver_release(struct inode *inode, struct file *file) {


	gpio_free(outputPin);
	// gpio_free(inputPin);
	Device_Open--;
	module_put(THIS_MODULE);
	return 0;
}

// not reading a pin in this case so the folowing is not needed but we will return 1 if pin is output
// return 0 if not
static ssize_t led_driver_read(struct file *filp, char *buffer, size_t length,   /* The length of the buffer     */
   							loff_t *offset)
{

	int bytes_read = 1;
	
	if(gpioRequestOutput == 0 && gpioDirectionRequestOutput == 0) 
	{
		return bytes_read;;
	}

	else return 0;
	
}

// Called when a process writes to dev file.
static ssize_t led_driver_write(struct file *filp, const char *buff, size_t len, loff_t *off) {

   	int bytes_written = 1;

	return bytes_written;
}

static long led_driver_ioctl(struct file *f, unsigned int cmd, unsigned long arg) 
{

    switch (cmd) 
    {

		case 0:

			printk("Case 0.\n");

			// Drive the output pin high to turn off the LED (if it was available and initialized).
			if(gpioRequestOutput == 0 && gpioDirectionRequestOutput == 0) 
			{
				gpio_set_value(outputPin, 1);
			}

			return 0;

		case 1:

			printk("Case 1.\n");

			// Drive the output pin low to turn on the LED (if it was available and initialized).
			if(gpioRequestOutput == 0 && gpioDirectionRequestOutput == 0) {
				gpio_set_value(outputPin, 0);
			}

			return 0;

		default:
	            return -EINVAL;
    }

    return 0;
}

static struct file_operations fops = {
	.owner = THIS_MODULE,
	.open = led_driver_open,
	.release = led_driver_release,
	.read = led_driver_read,
	.write = led_driver_write,
	.unlocked_ioctl = led_driver_ioctl
};

int init_module(void) {

	Major = register_chrdev(0, DEVICE_NAME, &fops);

	if (Major < 0) {

		printk ("Registering the character device failed with %d\n", Major);

		return Major;
	}


	if(request_mem_region(PORT, RANGE, DEVICE_NAME) == NULL) {

		printk("request_mem_region failed!\n");

		unregister_chrdev(Major, DEVICE_NAME);

		return -ENODEV;
	}

  return SUCCESS;
}

void cleanup_module(void) 
{

	release_mem_region(PORT, RANGE);


	unregister_chrdev(Major, DEVICE_NAME);

}
